/**
 * Template für Übungsaufgabe VS1lab/Aufgabe3
 * Das Skript soll die Serverseite der gegebenen Client Komponenten im
 * Verzeichnisbaum implementieren. Dazu müssen die TODOs erledigt werden.
 */

/**
 * Definiere Modul Abhängigkeiten und erzeuge Express app.
 */

var http = require('http');
//var path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');
var express = require('express');

var app;
app = express();
app.use(logger('dev'));
app.use(bodyParser.urlencoded({
	extended: false
}));

// Setze ejs als View Engine
app.set('view engine', 'ejs');

/**
 * Konfiguriere den Pfad für statische Dateien.
 * Teste das Ergebnis im Browser unter 'http://localhost:3000/'.
 */
app.use(express.static('public'));

app.use(bodyParser.json());

/**
 * Konstruktor für GeoTag Objekte.
 * GeoTag Objekte sollen min. alle Felder des 'tag-form' Formulars aufnehmen.
 */
function geoTagConstruct(name, latitude, longitude, hashtag){
	this.name = name;
	this.latitude = latitude;
	this.longitude = longitude;
	this.hashtag = hashtag;
}

// TODO: CODE ERGÄNZEN

/**
 * Modul für 'In-Memory'-Speicherung von GeoTags mit folgenden Komponenten:
 * - Array als Speicher für Geo Tags.
 * - Funktion zur Suche von Geo Tags in einem Radius um eine Koordinate.
 * - Funktion zur Suche von Geo Tags nach Suchbegriff.
 * - Funktion zum hinzufügen eines Geo Tags.
 * - Funktion zum Löschen eines Geo Tags.
 */

var geoTag = {
	geoTags:[],
	searchForRadius: function(latitude, longitude, radius){
		var localTags = [];
		for(var i = 0; i < this.geoTags.length; i++){
			if(this.geoTags[i] !== undefined){
				var lat = parseFloat(this.geoTags[i].latitude);
				var long = parseFloat(this.geoTags[i].longitude);
				if((lat >= latitude - radius && lat <= latitude+radius) && (long >= longitude - radius && long <= longitude+radius)){
					localTags.push(this.geoTags[i]);
				}
			}
		}
		return localTags;
	},
	searchForWord: function(searchterm){
		var localTags = [];
		for(var i = 0; i < this.geoTags.length; i++){
			if(this.geoTags[i] !== undefined){
				if(this.geoTags[i].name === searchterm){
					localTags.push(this.geoTags[i]);
				}
			}
		}
		return localTags;
	},
	addGeoTag: function(name, latitude, longitude, hashtag){
		this.geoTags.push(new geoTagConstruct(name, latitude, longitude, hashtag));
	},
	deleteGeoTag: function(index){
		delete this.geoTags[index];
	}
};

// TODO: CODE ERGÄNZEN

/**
 * Route mit Pfad '/' für HTTP 'GET' Requests.
 * (http://expressjs.com/de/4x/api.html#app.get.method)
 *
 * Requests enthalten keine Parameter
 *
 * Als Response wird das ejs-Template ohne Geo Tag Objekte gerendert.
 */

app.get('/', function(req, res) {
	res.render('gta', {
		taglist: geoTag.geoTags,
		position: []
	});
	res.end();
});

/**
 * Route mit Pfad '/tagging' für HTTP 'POST' Requests.
 * (http://expressjs.com/de/4x/api.html#app.post.method)
 *
 * Requests enthalten im Body die Felder des 'tag-form' Formulars.
 * (http://expressjs.com/de/4x/api.html#req.body)
 *
 * Mit den Formulardaten wird ein neuer Geo Tag erstellt und gespeichert.
 *
 * Als Response wird das ejs-Template mit Geo Tag Objekten gerendert.
 * Die Objekte liegen in einem Standard Radius um die Koordinate (lat, lon).
 */
app.post('/tagging', function(req, res){
	geoTag.addGeoTag(req.body.name, req.body.lat, req.body.long, req.body.hash);
	res.send(geoTag.geoTags);
	res.end();
});

/**
 * Route mit Pfad '/discovery' für HTTP 'POST' Requests.
 * (http://expressjs.com/de/4x/api.html#app.post.method)
 *
 * Requests enthalten im Body die Felder des 'filter-form' Formulars.
 * (http://expressjs.com/de/4x/api.html#req.body)
 *
 * Als Response wird das ejs-Template mit Geo Tag Objekten gerendert.
 * Die Objekte liegen in einem Standard Radius um die Koordinate (lat, lon).
 * Falls 'term' vorhanden ist, wird nach Suchwort gefiltert.
 */
app.get('/discovery/:searchterm', function(req, res){
	var filtered = geoTag.searchForWord(req.params.searchterm);
	res.send(filtered);
	res.end();
});

app.get('/discovery/:lat/:long', function(req, res){
	var filtered = geoTag.searchForRadius(req.params.lat, req.params.long, 5);
	res.send(filtered);
	res.end();
});

app.get('/getAll', function(req, res){
	res.send(geoTag.geoTags);
	res.end();
});

/**
 * Setze Port und speichere in Express.
 */

var port = 3000;
app.set('port', port);

/**
 * Erstelle HTTP Server
 */

var server = http.createServer(app);

/**
 * Horche auf dem Port an allen Netzwerk-Interfaces
 */

server.listen(port);
