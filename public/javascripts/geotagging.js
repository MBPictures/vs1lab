/* Dieses Skript wird ausgeführt, wenn der Browser index.html lädt. */

// Befehle werden sequenziell abgearbeitet ...

/**
 * "console.log" schreibt auf die Konsole des Browsers
 * Das Konsolenfenster muss im Browser explizit geöffnet werden.
 */
console.log("The script is going to start...");

// Es folgen einige Deklarationen, die aber noch nicht ausgeführt werden ...

/**
 * GeoTagApp Locator Modul
 */
var gtaLocator = (function GtaLocator() {

    // Private Member

    /**
     * Funktion spricht Geolocation API an.
     * Bei Erfolg Callback 'onsuccess' mit Position.
     * Bei Fehler Callback 'onerror' mit Meldung.
     * Callback Funktionen als Parameter übergeben.
     */
	 
    var tryLocate = function (onsuccess, onerror) {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(onsuccess, function (error) {
                var msg;
                switch (error.code) {
                    case error.PERMISSION_DENIED:
                        msg = "User denied the request for Geolocation.";
                        break;
                    case error.POSITION_UNAVAILABLE:
                        msg = "Location information is unavailable.";
                        break;
                    case error.TIMEOUT:
                        msg = "The request to get user location timed out.";
                        break;
                    case error.UNKNOWN_ERROR:
                        msg = "An unknown error occurred.";
                        break;
                }
                onerror(msg);
            });
        } else {
            onerror("Geolocation is not supported by this browser.");
        }
    };

    // Auslesen Breitengrad aus der Position
    var getLatitude = function (position) {
        return position.coords.latitude;
    };

    // Auslesen Längengrad aus Position
    var getLongitude = function (position) {
        return position.coords.longitude;
    };

    // Hier Google Maps API Key eintragen
    var apiKey = "AIzaSyDVmsWCMdHhxweCoHFxrKfp2Ju-jnKxDX8";

    /**
     * Funktion erzeugt eine URL, die auf die Karte verweist.
     * Falls die Karte geladen werden soll, muss oben ein API Key angegeben
     * sein.
     *
     * lat, lon : aktuelle Koordinaten (hier zentriert die Karte)
     * tags : Array mit Geotag Objekten, das auch leer bleiben kann
     * zoom: Zoomfaktor der Karte
     */
    var getLocationMapSrc = function (lat, lon, tags, zoom) {
        zoom = typeof zoom !== 'undefined' ? zoom : 10;

        if (apiKey === "YOUR API KEY HERE") {
            console.log("No API key provided.");
            return "images/mapview.jpg";
        }

        var tagList = "";
        if (typeof tags !== 'undefined') tags.forEach(function (tag) {
            tagList += "&markers=label:" + tag.name
                + "%7C" + tag.latitude + "," + tag.longitude;
        });

        var urlString = "http://maps.googleapis.com/maps/api/staticmap?center="
            + lat + "," + lon + "&markers=%7Clabel:you%7C" + lat + "," + lon
            + tagList + "&zoom=" + zoom + "&size=640x480&sensor=false&key=" + apiKey;

        console.log("Generated Maps Url: " + urlString);
        return urlString;
    };

    return { // Start öffentlicher Teil des Moduls ...

        // Public Member

        readme: "Dieses Objekt enthält 'öffentliche' Teile des Moduls.",

        updateLocation: function () {
			var taglist = $('#result-img').data("tags");
			if($('#latitude').val() === "" && $('#longitude').val() === ""){
				tryLocate(function(pos){
					var crd = pos.coords;
					$('#longitude').val(crd.longitude);
					$('#latitude').val(crd.latitude);
					console.log('Your current position is:');
					console.log('Latitude :' + crd.latitude);
					console.log('Longitude:' + crd.longitude);
					console.log('More or less ' + crd.accuracy + ' meters');
					$('#result-img').attr("src", getLocationMapSrc(crd.latitude, crd.longitude, taglist));
				}, function(data){
					alert(data);
				});
			}
			else{
				$('#result-img').attr("src", getLocationMapSrc($('#latitude').val(), $('#longitude').val(), taglist));
			}
			
        }

    }; // ... Ende öffentlicher Teil
})();

/**
 * $(document).ready wartet, bis die Seite komplett geladen wurde. Dann wird die
 * angegebene Funktion aufgerufen. An dieser Stelle beginnt die eigentliche Arbeit
 * des Skripts.
 */
function geoTagConstruct(name, latitude, longitude, hashtag){
	this.name = name;
	this.latitude = latitude;
	this.longitude = longitude;
	this.hashtag = hashtag;
}

$(document).ready(function () {
	gtaLocator.updateLocation();
    
	$('#submit-tagging').click(function(){
		$.ajax({
			type:"post",
			url: "/tagging",
			contentType: 'application/json',
			data:JSON.stringify({name: $("#name").val(), lat: $("#latitude").val(), long: $("#longitude").val(), hash:$("#hashtag").val()}),
			success: function(data){
				$("#results").empty();
				$.each( data, function( key, val ) {
					$("#results").append("<li>"+val.name+" ("+val.latitude+", "+val.longitude+") "+val.hashtag+"</li>");
				});
				gtaLocator.updateLocation();
			}
		});
	});
	
	$('#submit-discovery').click(function(){
		var param = "discovery/";
		var mode = "Filter: ";
		if($('#searchterm').val() !== ""){
			param += $('#searchterm').val();
			mode += "'" + $('#searchterm').val() + "'";
		}
		else{
			if($("#latitude").val() === "" || $("#longitude").val() === ""){
				mode += "Position konnte nicht erfasst werden!";
			}
			else{
				param += $("#latitude").val() + "/" + $("#longitude").val();
				mode += "Radius 5 um aktuelle Position";
			}
		}
		$("#filtered").html(mode);
		$.get(param, function(data){
			$("#results").empty();
			$.each( data, function( key, val ) {
				$("#results").append("<li>"+val.name+" ("+val.latitude+", "+val.longitude+") "+val.hashtag+"</li>");
			});
			gtaLocator.updateLocation();
		});
	});
	
	$("#remove-discovery").click(function(){
		$.get("/getALl", function(data){
			$("#results").empty();
			$.each( data, function( key, val ) {
				$("#results").append("<li>"+val.name+" ("+val.latitude+", "+val.longitude+") "+val.hashtag+"</li>");
			});
			$("#filtered").html("");
		});
	});
});

function updateGeoLoc(){
	
}